library(lavaan)

op_aut <- read.csv("op_autData.csv")

# the starting model

op_aut.1f <- ' 
 op_aut =~ op_aut3 + op_aut2 + op_aut1 + op_aut4 + op_aut5 + op_aut6
'
# configurational invariance 
fit_configural <- cfa(op_aut.1f, data=op_aut, ordered=paste0("op_aut", 1:6), group = "education")
summary(fit_configural, fit.measures = TRUE,  modindices = TRUE, standardized=TRUE, rsq=TRUE)

# weak invariance 
fit_weak <- cfa(op_aut.1f, data=op_aut, ordered=paste0("op_aut", 1:6), group = "education", group.equal = c("loadings"))
summary(fit_weak, fit.measures = TRUE,  modindices = TRUE, standardized=TRUE, rsq=TRUE)
lavTestLRT(fit_configural, fit_weak, method="satorra.bentler.2010")

#weak invariance - partial invariance for op_aut1

fit_weak2 <- cfa(op_aut.1f, data=op_aut, ordered=paste0("op_aut", 1:6), group = "education", group.equal = c("loadings"), group.partial = c("op_aut=~ op_aut1"))
summary(fit_weak2, fit.measures = TRUE,  modindices = TRUE, standardized=TRUE, rsq=TRUE)
lavTestLRT(fit_configural, fit_weak2, method="satorra.bentler.2010")

#strong invariance

fit_strong <- cfa(op_aut.1f, data=op_aut, ordered=paste0("op_aut", 1:6), group = "education", group.equal = c("loadings", "thresholds"), group.partial = c("op_aut=~ op_aut1"))
summary(fit_strong, fit.measures = TRUE,  modindices = TRUE, standardized=TRUE, rsq=TRUE)
lavTestLRT(fit_strong, fit_weak2, method="satorra.bentler.2010")

#strict invariance (WARNING: it does not converge)
fit_strict <- cfa(op_aut.1f, data=op_aut, ordered=paste0("op_aut", 1:6), group = "education", group.equal = c("loadings", "intercepts", "residuals", "residual.covariances"), group.partial = c("op_aut=~ op_aut1"))
summary(fit_strict, fit.measures = TRUE,  modindices = TRUE, standardized=TRUE, rsq=TRUE)
lavTestLRT(fit_strict, fit_strong, method="satorra.bentler.2010")


